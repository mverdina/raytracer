#define _USE_MATH_DEFINES
#include <math.h>

#include "material.h"

#include "sphere.h"

Sphere::Sphere(const Vector3D& center, float radius, Material* material)
	: mCenter(center),
		mRadius(radius),
		mMaterial(material),
		mIsLight(false)
{
}

Sphere::~Sphere()
{
	delete mMaterial;
}

Intersection Sphere::intersect(const Ray& ray)
{
	Vector3D l = mCenter - ray.getOrigin();
	float t_ca = dot(l, ray.getDirection());

	if (t_ca < 0.0f)
		return Intersection(false);

	float h = dot(l, l) - t_ca * t_ca;
	if (h > mRadius * mRadius)
		return Intersection(false);
	float t_hc = sqrt(mRadius * mRadius - h);
	float t0 = t_ca - t_hc;
	float t1 = t_ca + t_hc;

	Intersection res(true);
	float rayEnter = -1.f;
	float rayExit = -1.f;
	if (t0 >= 0.f)
	{
		rayEnter = t0;
		res.Distances.push_back(t0);
	}
	
	if (t1 >= 0.f)
	{
		res.Distances.push_back(t1);
		if (rayEnter < 0.f)
		{
			rayEnter = t1;
		}
		else if (t1 < rayEnter)
		{
			rayExit = rayEnter;
			rayEnter = t1;
		}
		else
		{
			rayExit = t1;
		}
	}

	if (rayEnter > 0.f)
	{
		res.Distance = rayEnter;
		res.Object	 = this;
		res.Normal	 = getNormal(ray, rayEnter);
		if (rayExit < 0.f)
		{
			res.Distances.insert(res.Distances.begin(), 0.f);
		}

		return res;
	}
	return Intersection(false);

}



Vector3D Sphere::getNormal(const Ray& ray, float distance, const Intersection& isect) const
{
	Vector3D normal = (ray.apply(distance) - mCenter) / mRadius;
	normal.normalize();
	return normal;
}


Color Sphere::getAmbientColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->AmbientColor;
}

Color Sphere::getDiffuseColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->DiffuseColor;
}

Color Sphere::getSpecularColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->SpecularColor;
}