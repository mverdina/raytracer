#include "csgtree.h"

CSGTree::CSGTree(CSGNode* root)
	: mRoot(root)
{

}

CSGTree::~CSGTree()
{
	if (mRoot)
	{
		mRoot->removeChildren();
		delete mRoot;
	}
}

Intersection CSGTree::intersect(const Ray& ray) 
{
	return mRoot->intersect(ray);
}

const Material* CSGTree::getMaterial() const
{
	return NULL;
}

Vector3D CSGTree::getNormal(const Ray& ray, float distance, const Intersection& isect) const
{
	return isect.Normal;
}

void CSGTree::setIsLight(bool)
{

}

bool CSGTree::isLight() const
{
	return false;
}

Color CSGTree::getAmbientColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getAmbientColor(point, isect);
}


Color CSGTree::getDiffuseColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getDiffuseColor(point, isect);
}


Color CSGTree::getSpecularColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getSpecularColor(point, isect);
}


CSGOperand::CSGOperand(CSGNode* lh, CSGNode* rh)
	: mLeftHand(lh),
		mRightHand(rh)
{
}

CSGOperand::~CSGOperand()
{
	removeChildren();
}

void CSGOperand::removeChildren()
{

}


CSGValue::CSGValue(IShape* shape)
	: CSGOperand(NULL, NULL),
		mShape(shape)
{
}

CSGValue::~CSGValue()
{
	delete mShape;
	mShape = NULL;
}

Intersection CSGValue::intersect(const Ray& ray)
{
	return mShape->intersect(ray);
}

const Material* CSGValue::getMaterial() const
{
	return mShape->getMaterial();
}

Vector3D CSGValue::getNormal(const Ray& ray, float distance, const Intersection& isect/* = Intersection() */) const
{
	return mShape->getNormal(ray, distance, isect);
}

void CSGValue::setIsLight(bool light)
{
	mShape->setIsLight(light);
}

bool CSGValue::isLight() const
{
	return mShape->isLight();
}

Color CSGValue::getAmbientColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	return mShape->getAmbientColor(point, isect);
}


Color CSGValue::getDiffuseColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	return mShape->getDiffuseColor(point, isect);
}


Color CSGValue::getSpecularColor(const Vector3D& point, const Intersection& isect /*= Intersection()*/) const
{
	return mShape->getSpecularColor(point, isect);
}
