#ifndef XMLSERIALIZE_H
#define XMLSERIALIZE_H

#include <QDomDocument>
#include <QDomElement>
#include <QDomNode>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QVector>

// Author: Alexey Vinogradov
struct IXmlSerializable
{
	//! Add some custom static template methods to simplify reading/writing

	/*!
		*\brief Find node with given name at child list of given parent
		*\param parent Parent node
		*\param name		Node name
		*\return Found node or null one, if desired node not found
		*/
	static QDomNode findNode(const QDomNode& parent, const QString& name);

	/*!
		*\brief Specialized template forEach visitor for readers, that can have several entities
		*\      Subclass object must type definitions for entities container as VisitEntities and for single entity VisitEntity.
		*\      Also it must implement method getEntities, returning const reference to VisitEntities.
		*\param object  Xml serializable subclass instance
		*\param visitor Template visitor functor reference, that must implmenent operator() for specific entity type
		*/
	template <class Subclass, class T>
	static void forEach(const Subclass& object, T& visitor)
	{
		const Subclass::VisitEntities& entities = object.getEntities();

		foreach (const Subclass::VisitEntity& entity, entities)
			visitor(entity);
	}

	/**
		* Read methods
		*/

	/*!
		*\brief Read node value
		*\param Node reference
		*\param Out value reference
		*\return true - succeeded, false otherwise
		*/
	template <class T>
	static bool readValue(const QDomNode& node, T& value);

	/*!
		*\brief Read node attribute value
		*\param Node reference
		*\param Attribute name
		*\param Out value reference
		*\return true - succeeded, false otherwise
		*/
	template <class T>
	static bool readAttribute(const QDomNode& node, const QString& attrName, T& value);

	
	/*!
		*\brief Read set of nodes
		*\param Node reference
		*\param Out vector of values reference
		*\return true - succeeded, false otherwise
		*/
	template <class T>
	static bool readArray(const QDomNode& node, QVector< T >& value);

	/*!
		*\brief Read sibling nodes array
		*\param node  Node reference
		*\param value Out vector of values reference
		*\return True - succeeded, false otherwise
		*/
	template <class T>
	static bool readSiblingArray(const QDomNode& node, QVector< T >& value);

	/*!
		*\brief Convert value of node into complex data
		*\param node Node reference
		*\param elem Out value
		*\return true - succeeded, false otherwise
		*/
	template <class T>
	static bool convert(const QDomNode& node, T& elem);

	/*!
		*\brief Convert string value into T type
		*\param string String
		*\param elem		Out value
		*\return True - succeeded, false otherwise
		*/
	template <class T>
	static bool convert(const QString& string, T& elem);

	/*!
		*\brief Read map of values - nodes with 2 attributes, where 1 is key, another is value
		*\param node			Parent node for mapped values
		*\param keyTag		Key tag name
		*\param valueTag Value tag name
		*\param map			Out - map of values
		*/
	template <class K, class T>
	static bool readMap(const QDomNode& node, const QString& keyTag, const QString& valueTag, QMap<K, T>& map);

	/**
		* Write methods
		*/

	/*!
		*\brief Add specific node as a child of given one
		*\param parent Parent node
		*\param node   New node to add
		*/
	static QDomNode addNode(QDomNode& parent, const QDomNode& node)
	{
		return parent.appendChild(node);
	}

	/*!
	*\brief Add specific element as a child of given one
	*\param parent  Parent node			 
	*\param element	New element to add
	*/
	static QDomNode addElement(QDomNode& parent, const QDomElement& element)
	{
		return addNode(parent, element);
	}

	/*!
		*\brief Write node T value
		*\param node  XML node to write
		*\param value T type value
		*\return true - succeeded, false otherwise
		*/
	template <typename T>
	static bool writeNodeValue(QDomDocument& document, QDomNode& node, const T& value);

	/*!
		*\brief Add node as child of given node with value
		*\param document XML document
		*\param parent Parent XML node
		*\param tag		Tag name
		*\param value	T type value of node to add
		*\return True - succeeded, false otherwise
		*/
	template <typename T>
	static QDomElement addElement(QDomDocument& document, QDomNode& parent, const QString& tag, const T& value);

	/*!
		*\brief Add node as child of given node
		*\param document XML document
		*\param parent Parent XML node
		*\param tag		Tag name
		*\return True - succeeded, false otherwise
		*/
	static QDomElement addElement(QDomDocument& document, QDomNode& parent, const QString& tag)
	{
		QDomElement deNewChild = document.createElement(tag);

		return addElement(parent, deNewChild).toElement();
	}

	/*! 
		*\brief Add node attribute
		*\param node  XML node for adding attribute
		*\param name  Attribute name
		*\param value T type value of attribute. All attributes are present as strings, so if needed, quotes must be added
		*\return True - succeeded, false otherwise
		*/
	template <typename T>
	static bool addAttribute(QDomElement& node, const QString& name, const T& value);

	/*!
		*\brief Convert T-typed element to element node
		*\param document Document node
		*\param node		  Dom element instance
		*\param element  Template element
		*/
	template <typename T>
	static bool convert(QDomDocument& document, QDomElement& node, const T& element);

	/*!
		*\brief Write map of values as dom elements with given parent
		*\param document  XML document instance
		*\param parent    Parent node
		*\param mapTag    Tag name of each map value
		*\param keyName   Desired name of the key attribute
		*\param valueName Desired name of the value attribute
		*\param map			 Map of elements
		*\return True - succeeded, false otherwise
		*/
	template <typename K, typename T>
	static bool writeMap(QDomDocument& document, QDomNode& node, const QString& mapTag, const QString& keyName, const QString& valueName, const QMap< K, T >& map);

	/*!
		*\brief Write vector of values as dom elements with given parent 
		*\param document XML document instance
		*\param parent   Parent node
		*\param tagName	Node's tag name
		*\param elements Vector of elements
		*/
	template <typename T>
	static bool writeContainer(QDomDocument& document, QDomNode& node, const QString& tagName, const QVector< T >& elements);

	/*!
		*\brief Destructor
		*/
	virtual ~IXmlSerializable()
	{

	}

	/*!
		*\brief Deserialize object from given node
		*\param node Xml node to read from
		*/
	virtual bool read(const QDomNode* node) = 0;

};

/**
	* Explicit specializations for reading common types
	*/

	template <>
	inline bool IXmlSerializable::readValue(const QDomNode& node, long& value)
	{
		if (node.isNull()) return false;

		value = node.nodeValue().toLong();

		return true;
	}

	template <>
	inline bool IXmlSerializable::readValue(const QDomNode& node, QString& value)
	{
		if (node.isNull()) return false;

		value = node.nodeValue();

		return true;
	}

	template <>
	inline bool IXmlSerializable::readAttribute(const QDomNode& node, const QString& attrName, int& value)
	{
		QDomNamedNodeMap attributes = node.attributes();

		QDomNode item = attributes.namedItem(attrName);

		if (!item.isNull())
		{
			value = item.nodeValue().toInt();
			return true;
		}

		return false;
	}

	template <>
	inline bool IXmlSerializable::readAttribute(const QDomNode& node, const QString& attrName, qint64& value)
	{
		QDomNamedNodeMap attributes = node.attributes();

		QDomNode item = attributes.namedItem(attrName);

		if (!item.isNull())
		{
			value = item.nodeValue().toLongLong();
			return true;
		}

		return false;
	}

	template <>
	inline bool IXmlSerializable::readAttribute(const QDomNode& node, const QString& attrName, uint& value)
	{
		QDomNamedNodeMap attributes = node.attributes();

		QDomNode item = attributes.namedItem(attrName);

		if (!item.isNull())
		{
			value = item.nodeValue().toUInt();
			return true;
		}

		return false;
	}

	template <>
	inline bool IXmlSerializable::readAttribute(const QDomNode& node, const QString& attrName, float& value)
	{
		QDomNamedNodeMap attributes = node.attributes();

		QDomNode item = attributes.namedItem(attrName);

		if (!item.isNull())
		{
			value = item.nodeValue().toFloat();
			return true;
		}

		return false;
	}

	template <>
	inline bool IXmlSerializable::readAttribute(const QDomNode& node, const QString& attrName, bool& value)
	{
		QDomNamedNodeMap attributes = node.attributes();

		QDomNode item = attributes.namedItem(attrName);

		if (!item.isNull())
		{
			value = item.nodeValue().toInt() == 1;
			return true;
		}

		return false;
	}

	template <>
	inline bool IXmlSerializable::readAttribute(const QDomNode& node, const QString& attrName, long& value)
	{
		QDomNamedNodeMap attributes = node.attributes();

		QDomNode item = attributes.namedItem(attrName);

		if (!item.isNull())
		{
			value = item.nodeValue().toLong();
			return true;
		}

		return false;
	}

	template <>
	inline bool IXmlSerializable::readAttribute(const QDomNode& node, const QString& attrName, QStringList& value)
	{
		QDomNamedNodeMap attributes = node.attributes();

		QDomNode item = attributes.namedItem(attrName);

		if (!item.isNull())
		{
			QString sJoined = item.nodeValue();
			value = sJoined.split(QChar(';'), QString::SkipEmptyParts);

			return true;
		}

		return false;
	}

	template <>
	inline bool IXmlSerializable::readAttribute(const QDomNode& node, const QString& attrName, QString& value)
	{
		QDomNamedNodeMap attributes = node.attributes();

		QDomNode item = attributes.namedItem(attrName);

		if (!item.isNull())
		{
			value = item.nodeValue();
			return true;
		}

		return false;
	}

	template <>
	inline bool IXmlSerializable::readAttribute(const QDomNode& node, const QString& attrName, QByteArray& value)
	{
		QDomNamedNodeMap attributes = node.attributes();

		QDomNode item = attributes.namedItem(attrName);

		if (!item.isNull())
		{
			value = item.nodeValue().toLocal8Bit();
			return true;
		}

		return false;
	}

	template <class T>
	inline bool IXmlSerializable::readArray(const QDomNode& node, QVector< T > &value)
	{
		QDomNode child = node.firstChild();

		while (!child.isNull())
		{
			T proxy;
			convert(child, proxy);
			value.push_back(proxy);

			child = child.nextSibling();
		}

		return true;
	}

	template <class T>
	inline bool IXmlSerializable::readSiblingArray(const QDomNode& node, QVector< T >& value)
	{
		QDomNode iterable = node;

		while (!iterable.isNull())
		{
			T proxy;

			convert(iterable.firstChild(), proxy);
			value.push_back(proxy);

			iterable = iterable.nextSibling();
		}

		return true;
	}

	template <>
	inline bool IXmlSerializable::convert(const QDomNode& node, QString& elem)
	{
		elem = node.nodeValue();
		return true;
	}

	template <>
	inline bool IXmlSerializable::convert(const QString& string, QString& elem)
	{
		elem = string;
		return true;
	}

	template <>
	inline bool IXmlSerializable::convert(const QString& string, int& elem)
	{
		elem = string.toInt();
		return true;
	}

	template <>
	inline bool IXmlSerializable::convert(const QString& string, uint& elem)
	{
		elem = string.toUInt();
		return true;
	}

	template <>
	inline bool IXmlSerializable::convert(const QString& string, float& elem)
	{
		elem = string.toFloat();
		return true;
	}

	template <typename K, typename T>
	inline bool IXmlSerializable::readMap(const QDomNode& node, const QString& keyTag, const QString& valueTag, QMap<K, T>& map)
	{
		QDomNode child = node.firstChild();

		while (!child.isNull())
		{
			QString key, value;
			readAttribute(child, keyTag, key);
			readAttribute(child, valueTag, value);

			K tKey;
			T tValue;

			convert(key, tKey);
			convert(value, tValue);

			map.insert(tKey, tValue);

			child = child.nextSibling();
		}

		return true;
	}


/**
	* Explicit specializations for writing common types
	*/

	template <>
	inline bool IXmlSerializable::writeNodeValue(QDomDocument& document, QDomNode& node, const int& value)
	{
		QDomText valueElement = document.createTextNode(QString::number(value));
		addNode(node, valueElement);
		return true;
	}

	template <>
	inline bool IXmlSerializable::writeNodeValue(QDomDocument& document, QDomNode& node, const float& value)
	{
		QDomText valueElement = document.createTextNode(QString::number(value));
		addNode(node, valueElement);
		return true;
	}

	template <>
	inline bool IXmlSerializable::writeNodeValue(QDomDocument& document, QDomNode& node, const QString& value)
	{
		QDomText valueElement = document.createTextNode(value);
		addNode(node, valueElement);
		return true;
	}


	template <>
	inline bool IXmlSerializable::writeNodeValue(QDomDocument& document, QDomNode& node, const bool& value)
	{
		QDomText valueElement = document.createTextNode(value ? "true" : "false");
		addNode(node, valueElement);
		return true;
	}

	template <typename T>
	inline QDomElement IXmlSerializable::addElement(QDomDocument& document, QDomNode& parent, const QString& name, const T& value)
	{
		QDomElement newChild = document.createElement(name);

		writeNodeValue(document, newChild, value);

		return addElement(parent, newChild).toElement();
	}

	template <typename T>
	inline bool IXmlSerializable::addAttribute(QDomElement& node, const QString& name, const T& value)
	{
		node.setAttribute(name, value);
		return true;
	}

	template <typename K, typename T>
	inline bool IXmlSerializable::writeMap(QDomDocument& document, QDomNode& parent, const QString& mapTag, const QString& keyName, const QString& valueName, const QMap< K, T >& map)
	{
		for (QMap< K, T >::const_iterator iElem = map.begin(); iElem != map.end(); ++iElem)
		{
			QDomElement element = addElement(document, parent, mapTag);

			addAttribute(element, keyName, iElem.key());
			addAttribute(element, valueName, iElem.value());
		}

		return true;
	}

	template <typename T>
	inline bool IXmlSerializable::writeContainer(QDomDocument& document, QDomNode& node, const QString& tagName, const QVector< T >& elements)
	{
		bool r = true;
		for (int idx = 0, count = elements.size(); idx < count; ++idx)
		{
			QDomElement element = addElement(document, node, tagName);

			r = r && convert(document, element, elements[idx]);
		}

		return r;
	}
		 
#endif // XMLSERIALIZE_H_