#ifndef SCENE_H_
#define SCENE_H_
	
#include <vector>

#include "intersection.h"

#include "types.h"

class	 Camera;
struct CameraProperties;
struct IShape;
struct LightSource;
struct Material;
class  Ray;
struct TracerProperties;
	

class Scene
{
public:
	static const Material* GetDefaultAirProperties();

public:
	explicit Scene();

	~Scene();

	Intersection intersect(const Ray& ray, bool stopIfFound) const;
	Color illuminate(const Ray& viewRay, IShape* object, float distance, const Vector3D& normal) const;

	void addObject(IShape* object);

	void addLightSource(LightSource* light);

	void setBackground(Material* bgMaterial);

	void setupCamera(const CameraProperties& properties);

	void setTracerProperties(TracerProperties* properties);

	void setImagePlaneRes(int resX, int resY);

	void clear();

	void setTracerDepth(float depth);

	const std::vector< IShape* > &getObjects() const
	{
		return mObjects;
	}

	const std::vector< LightSource* > &getLights() const
	{
		return mLights;
	}

	Material* const getBackground() const
	{
		return mBackground;
	}

	Camera* const getCamera() const
	{
		return mCamera;
	}

	TracerProperties* const getTracerProperties() const
	{
		return mTracerProperties;
	}

	int getImagePlaneWidth() const
	{
		return mImagePlaneWidth;
	}

	int getImagePlaneHeight() const
	{
		return mImagePlaneHeight;
	}

private:
	Scene(const Scene&);
	Scene& operator=(const Scene&);

private:
	std::vector< IShape* >	mObjects;
	std::vector< LightSource* > mLights;
	Material *mBackground;
	Camera *mCamera;
	TracerProperties *mTracerProperties;
	float mTracerDepth;
	int	mImagePlaneWidth;
	int	mImagePlaneHeight;
};

#endif // SCENE_H_