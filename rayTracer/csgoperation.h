#ifndef CSGOPERATION_H_
#define CSGOPERATION_H_

#include "csgtree.h"

	class CSGOperation : public CSGOperand
	{
	public:

		explicit CSGOperation(CSGNode* lh, CSGNode* rh)
			: CSGOperand(lh, rh)
		{
		}

	};

	class CSGUnion : public CSGOperation
	{
	public:
		explicit CSGUnion(CSGNode* lh, CSGNode* rh);

		virtual ~CSGUnion();

		virtual Intersection intersect(const Ray& ray);
		virtual const Material* getMaterial() const;
		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;
		virtual void setIsLight(bool light);
		virtual bool isLight() const;
		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	};

	class CSGIntersection : public CSGOperation
{
public:
	explicit CSGIntersection(CSGNode* lh, CSGNode* rh);

	virtual ~CSGIntersection();

	virtual Intersection intersect(const Ray& ray);
	virtual const Material* getMaterial() const;
	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;
	virtual void setIsLight(bool light);
	virtual bool isLight() const;
	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

};

class CSGDifference : public CSGOperation
{
public:
	explicit CSGDifference(CSGNode* lh, CSGNode* rh);

	virtual ~CSGDifference();

	virtual Intersection intersect(const Ray& ray);
	virtual const Material* getMaterial() const;
	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;
	virtual void setIsLight(bool light);
	virtual bool isLight() const;
	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
};


#endif //CSGOPERATION_H_