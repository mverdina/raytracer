#include <assert.h>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>

#include "ray.h"
#include "light.h"
#include "material.h"
#include "shape.h"

#include "camera.h"
#include "scene.h"

#include "tracer.h"

#define EXPOSURE_FACTOR  -1.0f
#define COMPONENTS_COUNT 4
#define RGBA(r, g, b, a) ((a & 0xff) << 24) | ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);

#define USE_SHLICK_APPROXIMATION

namespace 
{
	Vector3D GRefractRay(const Vector3D& sourceDirection, 
						float sourceEnvDensity, 
						float targetEnvDensity,
						const Vector3D& outNormal,
						bool* isTotalInternalReflection)
	{
		const float nue				 = sourceEnvDensity / targetEnvDensity;
		const float cosThetaS  = -dot(outNormal, sourceDirection);
		const float cosThetaT2 = 1.f - nue * nue * (1.f - cosThetaS * cosThetaS);

		if (cosThetaT2 < 0.f)
		{
			*isTotalInternalReflection = true;
			return Vector3D();
		}

		return ((nue * sourceDirection) + (nue * cosThetaS - sqrtf(cosThetaT2)) * outNormal).toUnit();
	}

	float GGetFresnelFactor(const Vector3D& sourceDirection, 
							float sourceEnvDensity, 
							float targetEnvDensity,
							const Vector3D& outNormal,
							bool* isTotalInternalReflection)
	{
		const float nue	 = sourceEnvDensity / targetEnvDensity;
		const float cosThetaS  = -dot(outNormal, sourceDirection);

		float cosThetaT2 = 1.f - nue * nue * (1.f - cosThetaS * cosThetaS);

		if (cosThetaT2 < 0.f)
		{
			*isTotalInternalReflection = true;
			return 0.f;
		}

		cosThetaT2 = sqrtf(cosThetaT2);

		float Rs = (sourceEnvDensity * cosThetaS - targetEnvDensity * cosThetaT2) /
			(sourceEnvDensity * cosThetaS + targetEnvDensity * cosThetaT2);

		Rs *= Rs;

		float Rp = (sourceEnvDensity * cosThetaT2 - targetEnvDensity * cosThetaS) / 
			(sourceEnvDensity * cosThetaT2 + targetEnvDensity * cosThetaS);

		Rp *= Rp;

		return (0.5f * (Rs + Rp));
	}

	float GGetFresnelFactorShlick(const Vector3D& sourceDirection, 
								float sourceEnvDensity, 
								float targetEnvDensity,
								const Vector3D& outNormal,
								bool* isTotalInternalReflection)
	{
		float r0;

		r0 = ((sourceEnvDensity - targetEnvDensity) * (sourceEnvDensity - targetEnvDensity)) / 
			((sourceEnvDensity + targetEnvDensity) * (sourceEnvDensity + targetEnvDensity));

		float dirDotNormal = 1 + dot(sourceDirection, outNormal);

		return (r0 + (1 - r0) * pow(dirDotNormal, 5));
	}
}


Tracer::Tracer()
	: mCurrentExposureFactor(-1.f)
{
}

Tracer::~Tracer()
{
}

void Tracer::render(const Scene& scene, unsigned char* image)
{
	const int	cImagePlaneWidth  = scene.getImagePlaneWidth();
	const int	cImagePlaneHeight = scene.getImagePlaneHeight();
	const float cAirRefraction		= Scene::GetDefaultAirProperties()->Refraction;

	Camera* const camera = scene.getCamera();

	unsigned* data = reinterpret_cast< unsigned* >(image);


	for (int y = 0; y < cImagePlaneHeight; ++y)
	{
		
		for (int x = 0; x < cImagePlaneWidth; ++x)
		{
			Ray					 ray   = camera->lookThrough(x, y);
			Intersection isect;
			Color				 pixel = compute(scene, ray, 0,	 1.f, cAirRefraction, &isect);

			float fRed = COLOR_R(pixel);
			float	fGreen = COLOR_G(pixel); 
			float fBlue = COLOR_B(pixel);
			if (camera->hasExposure())
			{
				postprocessColor(pixel, &fRed, &fGreen, &fBlue);
			}
			else
			{
				saturateColor(pixel, &fRed, &fGreen, &fBlue);
			}

			if (camera->hasGammaCorrection())
			{
				fRed	 = gammaCorrection(fRed);
				fGreen = gammaCorrection(fGreen);
				fBlue  = gammaCorrection(fBlue);
			}
			
			unsigned char red   = static_cast<unsigned char>(std::min<unsigned>(fRed * 255, 255));
			unsigned char green = static_cast<unsigned char>(std::min<unsigned>(fGreen * 255, 255)); 
			unsigned char blue  = static_cast<unsigned char>(std::min<unsigned>(fBlue * 255, 255));
			
			int index = y * cImagePlaneWidth + x;
			*(data + index) = RGBA(red, green, blue, 255);

			
		}
	}

	std::cout << "Progress: 100%; Rendering finished." << std::endl;
}

Color Tracer::compute(const Scene& scene, const Ray& ray, int recursionDepth, float reflectionIntensity, float sourceEnvDensity, Intersection *out)
{
	const int cMaxRecursionDepth = scene.getTracerProperties()->MaxRayRecursionDepth;

	if (cMaxRecursionDepth >= 0 && recursionDepth > cMaxRecursionDepth)
	{
		return Color();
	}
	
	bool	reflected = recursionDepth != 0;
	Color resultColor;	
	
	Intersection intersection = scene.intersect(ray, false);
	*out = intersection;
	if (!intersection.Exists)
	{
		if (reflected)
		{
			return resultColor;
		}
		return getBackgroundColor(scene);
	}

	IShape *object = intersection.Object; 
	if (object->isLight())
	{
		if (reflected)
		{
			return resultColor;
		}

		const Material* material = object->getMaterial();
		return material->AmbientColor + material->DiffuseColor + material->SpecularColor;
	}

  const Vector3D  isectPoint		 = ray.apply(intersection.Distance);
  const Material *objectMaterial = object->getMaterial();

	const Vector3D normal = intersection.Normal;
	resultColor += (scene.illuminate(ray, object, intersection.Distance, normal));

	const Vector3D& rayDirection = ray.getDirection();
	const float viewProjection   = dot(rayDirection, normal);
	Vector3D outNormal = normal;
	if (viewProjection > 0.f)
	{
		outNormal = -normal;
	}
	bool isTotalInternalReflection = false;
	float fresnel = GGetFresnelFactor(rayDirection, sourceEnvDensity, objectMaterial->Density, outNormal, &isTotalInternalReflection);

	if (objectMaterial->Reflection > 0.f && reflectionIntensity > EPSILON)
	{
		const Vector3D& rayDirection = ray.getDirection();
		Vector3D direction = rayDirection - 2.f * dot(rayDirection, outNormal) * outNormal;
		Intersection reflected;
		Color reflectedColor = (compute(scene, Ray(isectPoint + direction * EPSILON, direction), recursionDepth + 1, 
										reflectionIntensity * objectMaterial->Reflection, objectMaterial->Density, &reflected) * 
										reflectionIntensity * objectMaterial->Reflection * fresnel);
		 resultColor += scale3D(reflectedColor, objectMaterial->DiffuseColor);
	}
	if (objectMaterial->Refraction > 0.f)
	{
		const float viewProjection = dot(rayDirection, normal);
		const float density				 = objectMaterial->Density;

		const float nue						 = sourceEnvDensity / density;

		const Vector3D direction = GRefractRay(rayDirection, sourceEnvDensity, density, outNormal, &isTotalInternalReflection);

		if (!isTotalInternalReflection)
		{
			Intersection	 refractedIsect;
			Color refracted  = compute(scene, Ray(isectPoint + direction * EPSILON, direction), recursionDepth + 1, 
												  reflectionIntensity, density, &refractedIsect);
			if (refractedIsect.Exists)
			{
				Color absorbance   = (objectMaterial->DiffuseColor) * 0.15f * (-refractedIsect.Distance);
				Color transparency = Color(expf(COLOR_R(absorbance)), expf(COLOR_G(absorbance)), expf(COLOR_B(absorbance)));
				resultColor += scale3D(refracted,  transparency) * (1.f - fresnel);
			}
		}
	}
			
	return resultColor;
}

void Tracer::postprocessColor(const Color& color, float *r, float *g, float *b)
{
	*r = 1.f - expf(COLOR_R(color) * mCurrentExposureFactor);
	*g = 1.f - expf(COLOR_G(color) * mCurrentExposureFactor);
	*b = 1.f - expf(COLOR_B(color) * mCurrentExposureFactor);
}

void Tracer::saturateColor(const Color& color, float *r, float *g, float *b)
{
	*r = std::min(COLOR_R(color), 1.f);
	*g = std::min(COLOR_G(color), 1.f);
	*b = std::min(COLOR_B(color), 1.f);
}

float Tracer::gammaCorrection(float color)
{
	if (color < 0.0031308f)
	{
		return 12.92f * color;
	}
	else
	{
		return 1.055f * powf(color, 0.4166667f) - 0.055f; // Inverse gamma 2.4
	}
}

Color Tracer::getBackgroundColor(const Scene& scene)
{
	return scene.getBackground()->AmbientColor;
}

Ray Tracer::reflectRay(const Vector3D& reflectedOrigin, const Vector3D& source, const Vector3D& over)
{
	return Ray(reflectedOrigin, (source - 2 * dot(source, over) * over).toUnit());
}

void Tracer::calculateExposure(const Scene& scene)
{
	static const int   cAccumulationSize			 = 16;
	static const float cMediumPointWeight			 = 1.f / (cAccumulationSize * cAccumulationSize);
	static const float cDefaultTonemapMidpoint = 0.7f;

	const int		cImagePlaneWidth  = scene.getImagePlaneWidth();
	const int		cImagePlaneHeight = scene.getImagePlaneHeight();

	float exposure			 = -1.f;
	float accuracyFactor = static_cast<float>(std::max(cImagePlaneWidth, cImagePlaneHeight));

	accuracyFactor /= cAccumulationSize;

	float mediumPoint = 0.f;

	const Camera* camera = scene.getCamera();
	for (int y = 0; y < cAccumulationSize; ++y)
	{
		for (int x = 0; x < cAccumulationSize; ++x)
		{
			Ray					 viewRay = camera->lookThrough(x * accuracyFactor, y * accuracyFactor);
			Intersection isect;
			Color				 result = compute(scene, viewRay, 0, 1.f, 1.f, &isect);

			float	luminance = 0.2126f   * COLOR_R(result) + 
												0.71516f  * COLOR_G(result) +
												0.072169f * COLOR_B(result);
			mediumPoint += cMediumPointWeight * (luminance * luminance);
		}
	}

	float mediumLuminance = sqrtf(mediumPoint);

	if (mediumLuminance > 0.f)
	{
		exposure = logf(1.f - cDefaultTonemapMidpoint) / mediumLuminance;
	}

	mCurrentExposureFactor = exposure;
}
