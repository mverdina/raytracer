#ifndef TYPES_H_
#define TYPES_H_

#include "vector3D.h"
typedef Vector3D Color;

#define COLOR_R(v) (v).x()
#define COLOR_G(v) (v).y()
#define COLOR_B(v) (v).z()
#define COLOR_SET_R(v, r) (v).setX((r))
#define COLOR_SET_G(v, g) (v).setY((g))
#define COLOR_SET_B(v, b) (v).setZ((b))


#endif // TYPES_H_