#ifndef CYLINDER_H_
#define CYLINDER_H_

#include "shape.h"

#include "ray.h"
#include "vector3d.h"

class Cylinder : public IShape
{
public:
  Cylinder(const Vector3D& top, const Vector3D& bottom, float radius, Material* material);

  virtual ~Cylinder();
  virtual Intersection intersect(const Ray& ray);

  virtual const Material* getMaterial() const
  {
    return mMaterial;
  }

  virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

  virtual void setIsLight(bool light)
  {
    mIsLight = light;
  }

  virtual bool isLight() const
  {
    return mIsLight;
  }

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
private:
  Vector3D	mBottom;
  Vector3D	mTop;
  float     mRadius;
  Vector3D  mAxis;
  Material* mMaterial;
  bool			mIsLight;
};

#endif // CYLINDER_H_