#ifndef PRECISION_H_
#define PRECISION_H_

#define FLOAT_ZERO 0.000001f
#define EPSILON		 0.0005f

#endif // PRECISION_H_