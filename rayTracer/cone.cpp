#include "material.h"

#include "cone.h"

Cone::Cone(const Vector3D& top, const Vector3D& bottom, float radius, Material* material)
	: mTop(top),
		mBottom(bottom),
		mRadius(radius),
		mMaterial(material),
		mIsLight(false)
{
	mAxis = (mTop - mBottom).toUnit();
	mRadPerHeight = mRadius / length(mTop - mBottom);
}

Cone::~Cone()
{
	delete mMaterial;
}

Intersection Cone::intersect(const Ray& ray)
{
	const Vector3D& CO = ray.getOrigin() - mBottom;

	const float dirDotAxis = dot(ray.getDirection(), mAxis);
	const float CODotAxis	 = dot(CO, mAxis);

	const Vector3D& u = ray.getDirection() + mAxis * (-dirDotAxis);
	const Vector3D& v = CO + mAxis * (-CODotAxis);
	const float     w = CODotAxis * mRadPerHeight;

	const float	radPerDir = dirDotAxis * mRadPerHeight;

	Intersection res = Intersection(true);

	const float a = dot(u, u) - radPerDir * radPerDir;
	float rayEnter = -1.f;
	float t;
	float rayExit = -1.f; 
	if (fabs(a) > FLOAT_ZERO)
	{
		const float b = 2 * (dot(u, v) - w * radPerDir);
		const float c = dot(v, v) - w * w;

		float D = b * b - 4 * a * c;

		if (D < 0.f) 
		{
			return Intersection(false);
		}

		t = (-b - sqrt(D)) / 2 / a;
		if (t > 0.f)
		{
			const Vector3D& surfacePoint = ray.apply(t);
			Vector3D toBottom = surfacePoint - mBottom;
			Vector3D toTop		= surfacePoint - mTop;
			if (dot(mAxis, toBottom) > 0.f && dot((-mAxis), toTop) > 0.f)
			{
				res.Distances.push_back(t);
				rayEnter = t;
			}
		}
		t = (-b + sqrt(D)) / 2 / a;
		if (t > 0.f)
		{
			const Vector3D& surfacePoint = ray.apply(t);
			Vector3D toBottom = surfacePoint - mBottom;
			Vector3D toTop		= surfacePoint - mTop;
			if (dot(mAxis, toBottom) > 0.f && dot(-mAxis, toTop) > 0.f)
			{
				res.Distances.push_back(t);
				if (rayEnter < 0.f)
				{
					rayEnter = t;
				}
				else if (t < rayEnter)
				{
					rayExit = rayEnter;
					rayEnter = t;
				}
				else
				{
					rayExit = t;
				}
			}
		}
	}


	if (fabs(dirDotAxis) < FLOAT_ZERO)
	{
		if (rayEnter > 0.f)
		{
			res.Exists	 = true;
			res.Distance = rayEnter;
			res.Object   = this;
			res.Normal	 = getNormal(ray, rayEnter);
			if (rayExit < 0.f) 
			{
				res.InsideIntervals.push_back(Span(0.f, rayEnter));
				res.Distances.insert(res.Distances.begin(), 0.f);
			}
			else
			{
				res.InsideIntervals.push_back(Span(rayEnter, rayExit));
			}
			
			return res;
		}

		return Intersection(false);
	}

	t = (dot(-mAxis, ray.getOrigin() - mTop)) / dirDotAxis;
	if (t > 0.f)
	{
		Vector3D test = ray.apply(t) - mTop;
		if (dot(test, test) < mRadius * mRadius)
		{
			res.Distances.push_back(t);
			if (rayEnter < 0.f)
			{
				rayEnter = t;
				rayExit = t;
			}
			else if (t < rayEnter)
			{
				rayExit = rayEnter;
				rayEnter = t;
			}
			else
			{
				rayExit = t;
			}
		}
	}
	
	if (rayEnter > 0.f)
	{
			res.Exists	 = true;
			res.Distance = rayEnter;
			res.Object   = this;
			res.Normal	 = getNormal(ray, rayEnter);
			if (rayExit < 0.f) 
			{
				res.InsideIntervals.push_back(Span(0.f, rayEnter));
				res.Distances.insert(res.Distances.begin(), 0.f);
			}
			else
			{
				res.InsideIntervals.push_back(Span(rayEnter, rayExit));
			}
			return res;
	}
	return Intersection(false);
}


Vector3D Cone::getNormal(const Ray& ray, float distance, const Intersection& isect /*= Intersection()*/) const
{
	const Vector3D atSurface = ray.apply(distance);
	const Vector3D& toBottom		 = atSurface - mTop;
	if (fabs(dot(mAxis, toBottom)) < FLOAT_ZERO && dot(toBottom, toBottom) < mRadius * mRadius)
	{
		return mAxis;
	}
	
	const Vector3D& approxNorm = atSurface - (mAxis * (dot(atSurface - mTop, mAxis)) + mTop);
	return (approxNorm + mAxis * (-mRadPerHeight * length(approxNorm))).toUnit();
}

Color Cone::getAmbientColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->AmbientColor;
}

Color Cone::getDiffuseColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->DiffuseColor;
}

Color Cone::getSpecularColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->SpecularColor;
}
