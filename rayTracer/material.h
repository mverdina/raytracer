#ifndef MATERIAL_H_
#define MATERIAL_H_

#include "types.h"
	
struct Material
{
	Color						EmissiveColor;
	Color						AmbientColor;	 
	Color						DiffuseColor;
	Color						SpecularColor;
	float						SpecularPower;

	float						Refraction;
	float						Reflection;
	float						Density;
	float						Illumination;
};

#endif // MATERIAL_H_