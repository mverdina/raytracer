#include "light.h"
#include "material.h"
#include "shape.h"
#include "camera.h"
#include "tracer.h"

#include "scene.h"

#define TOO_FAR_AWAY		 1000000.f

const Material* Scene::GetDefaultAirProperties()
{
	static Material cAir;
	
	cAir.Refraction = 1.f;			
	cAir.Reflection = 0.f;			
	cAir.Density    =	1.f;			
	
	return &cAir;
}

Scene::Scene()
	: mBackground(NULL),
		mCamera(NULL),
		mTracerProperties(NULL),
		mTracerDepth(0.f)
{
}

Scene::~Scene()
{
	clear();
}

Intersection Scene::intersect(const Ray& ray, bool stopIfFound) const
{
	float	closestDistance = TOO_FAR_AWAY;
	bool	anyFound				 = false;
	Intersection closestIntersection(false);

	for (int obj = 0, count = mObjects.size(); obj < count; ++obj)
	{
		Intersection isect = mObjects[obj]->intersect(ray);
		if (!isect.Exists)
		{
			continue;
		}
		if (mTracerDepth > 0.f && isect.Distance > mTracerDepth)
		{
			continue;
		}

		if (isect.Distance < closestDistance || !anyFound)
		{
			closestDistance	= isect.Distance;
			closestIntersection = isect;
			anyFound	= true;
		}

		if (stopIfFound)
		{
			return closestIntersection;
		}
	}

	return closestIntersection;
}

Color Scene::illuminate(const Ray& viewRay, IShape* object, float distance, const Vector3D& normal) const
{
	Color resultColor;
	for (int light = 0, count = mLights.size(); light < count; ++light)
	{
		const LightSource* source = mLights[light];
		resultColor += (source->computeColor(*this, object, viewRay, distance, normal));
	}
	return resultColor;
}

void Scene::addObject(IShape *object)
{
	mObjects.push_back(object);
}

void Scene::addLightSource(LightSource *light)
{
	mLights.push_back(light);
}

void Scene::setBackground(Material* bgMaterial)
{
	if (mBackground)
	{
		delete mBackground;
		mBackground = NULL;
	}
	mBackground = bgMaterial;
}

void Scene::setupCamera(const CameraProperties& properties)
{
	if (mCamera)
	{
		delete mCamera;
		mCamera = NULL;
	}
	mCamera = new Camera(properties);
}

void Scene::setTracerProperties(TracerProperties* properties)
{
	if (mTracerProperties)
	{
		delete mTracerProperties;
		mTracerProperties = NULL;
	}
	mTracerProperties = properties;
}

void Scene::setImagePlaneRes(int resX, int resY)
{
	mImagePlaneWidth  = resX;
	mImagePlaneHeight = resY;
	if (mCamera)
	{
		mCamera->setImagePlaneRes(resX, resY);
	}
}

void Scene::clear()
{
	for (int idx = 0, count = mObjects.size(); idx < count; ++idx)
	{
		delete mObjects[idx];
		mObjects[idx] = NULL;
	}
	mObjects.clear();
	for (int idx = 0, count = mLights.size(); idx < count; ++idx)
	{
		delete mLights[idx];
		mLights[idx] = NULL;
	}
	mLights.clear();

	delete mBackground;
	mBackground = NULL;
	delete mCamera;
	mCamera = NULL;
	delete mTracerProperties;
	mTracerProperties = NULL;
}

void Scene::setTracerDepth(float depth)
{
	mTracerDepth = depth;
}