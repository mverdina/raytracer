#ifndef TRACER_TRACER_H_
#define TRACER_TRACER_H_

#include <vector>

#include "intersection.h"
#include "types.h"

struct IShape;
class Scene;
class Ray;
struct TracerProperties
{
	int MaxRayRecursionDepth; 
	int MaxRayReflectionDepth;
};
	
class Tracer
{
public:
	Tracer();
	~Tracer();
	void render(const Scene& scene, unsigned char* image);

private:
	Color compute(const Scene& scene, const Ray& ray, int recursionDepth, float reflectionIntensity, float sourceEnvDensity, Intersection* out);
	void postprocessColor(const Color& color, float *r, float *g, float *b);
	void saturateColor(const Color& color, float *r, float *g, float *b);
	float gammaCorrection(float color);
	Color getBackgroundColor(const Scene& scene);
	Ray reflectRay(const Vector3D& reflectedOrigin, const Vector3D& source, const Vector3D& over);
	void calculateExposure(const Scene& scene);

private:
	float mCurrentExposureFactor;
};

#endif // TRACER_H_