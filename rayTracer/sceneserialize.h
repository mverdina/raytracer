#ifndef SCENESERIALIZABLE_H_
#define SCENESERIALIZABLE_H_

#include <QSharedPointer>

#include "xmlserialize.h"

class Scene;

class SceneSerializable : private IXmlSerializable
{
public:

	explicit SceneSerializable();

	virtual ~SceneSerializable();

	QSharedPointer< Scene > readScene(const QString& fileName);

protected:
	virtual bool read(const QDomNode* node);

private:
	QSharedPointer< Scene > mScene;
};

#endif // SCENESERIALIZABLE_H_
