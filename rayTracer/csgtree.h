#ifndef CSGTREE_H_
#define CSGTREE_H_
#include "intersection.h"
#include "shape.h"
	
	class Ray;

	struct CSGNode : IShape
	{
		virtual void removeChildren() = 0;
		virtual CSGNode* left() const = 0;
		virtual CSGNode* right() const = 0;
		virtual bool hasLeft() const = 0;
		virtual bool hasRight() const = 0;

	};

	class CSGTree : public IShape
	{
	public:
		explicit CSGTree(CSGNode* root);
		
		virtual ~CSGTree();

		virtual Intersection intersect(const Ray& ray);
		virtual const Material* getMaterial() const;
		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;
		virtual void setIsLight(bool light);
		virtual bool isLight() const;
		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	private:
		CSGNode* mRoot;
	};


	class CSGOperand : public CSGNode
	{
	public:
		explicit CSGOperand(CSGNode* lh, CSGNode* rh);

		~CSGOperand();

		virtual void removeChildren();

		virtual CSGNode* left() const
		{
			return mLeftHand;
		}

		virtual CSGNode* right() const
		{
			return mRightHand;
		}

		virtual bool hasLeft() const
		{
			return mLeftHand != NULL;
		}

		virtual bool hasRight() const
		{
			return mRightHand != NULL;
		}

	private:
		CSGNode *mLeftHand;
		CSGNode *mRightHand;
	};

	class CSGValue : public CSGOperand
	{
	public:
		explicit CSGValue(IShape* shape);

		virtual ~CSGValue();

		virtual Intersection intersect(const Ray& ray);
		virtual const Material* getMaterial() const;
		virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;
		virtual void setIsLight(bool light);
		virtual bool isLight() const;
		virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
		virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
		virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	private:
		IShape* mShape;
	};

#endif //CSGTREE_H_