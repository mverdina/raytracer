#ifndef PLANE_H_
#define PLANE_H_

#include "shape.h"

class Plane : public IShape
{
public:
	explicit Plane(const Vector3D& normal, float D, Material* material);

	virtual ~Plane();
	virtual Intersection intersect(const Ray& ray);

	virtual const Material* getMaterial() const
	{
		return mMaterial;
	}

	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const
	{
		return mNormal;
	}

	virtual void setIsLight(bool light)
	{
		mIsLight = light;
	}

	virtual bool isLight() const
	{
		return mIsLight;
	}

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

private:
	Vector3D	mNormal;
	float mD;
	Material *mMaterial;
	bool mIsLight;
};

#endif // PLANE_H_
