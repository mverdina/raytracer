#include <algorithm>
#include <xutility>

#include "csgoperation.h"

#define TOO_FAR_AWAY		 1000000.f 

CSGUnion::CSGUnion(CSGNode *lh, CSGNode *rh)
	: CSGOperation(lh, rh)
{
}

CSGUnion::~CSGUnion()
{
}

Intersection CSGUnion::intersect(const Ray& ray) 
{
	Intersection leftIsect  = left()->intersect(ray);
	Intersection rightIsect = right()->intersect(ray);

	if (!leftIsect.Exists)
	{
		leftIsect.Distance = TOO_FAR_AWAY;
	}
	if (!rightIsect.Exists)
	{
		rightIsect.Distance = TOO_FAR_AWAY;
	}

	Intersection unite;

	unite.Exists   = leftIsect.Exists || rightIsect.Exists;
	if (!unite.Exists)
	{
		return Intersection(false);
	}

	if (leftIsect.Distance < rightIsect.Distance)
	{
		unite.Distance  = leftIsect.Distance;
		unite.Normal	  = leftIsect.Normal;
		unite.Object	  = leftIsect.Object;
		unite.u					= leftIsect.u;
		unite.v					= leftIsect.v;
	}
	else
	{
		unite.Distance  = rightIsect.Distance;
		unite.Normal	  = rightIsect.Normal;
		unite.Object	  = rightIsect.Object;
		unite.u					= rightIsect.u;
		unite.v					= rightIsect.v;
	}
	
	if (leftIsect.Exists)
	{
		unite.Distances.resize(leftIsect.Distances.size());
		std::copy(leftIsect.Distances.begin(), leftIsect.Distances.end(), unite.Distances.begin());
	}
	if (rightIsect.Exists)
	{
		const int size = unite.Distances.size();
		unite.Distances.resize(unite.Distances.size() + rightIsect.Distances.size());
		std::copy(rightIsect.Distances.begin(), rightIsect.Distances.end(), unite.Distances.begin() + size);
	}

	return unite;
}

const Material* CSGUnion::getMaterial() const
{
	return NULL;
}

Vector3D CSGUnion::getNormal(const Ray& ray, float distance, const Intersection& isect) const
{
	return isect.Normal;
}

void CSGUnion::setIsLight(bool)
{
}

bool CSGUnion::isLight() const
{
	return false;
}

Color CSGUnion::getAmbientColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getAmbientColor(point, isect);
}


Color CSGUnion::getDiffuseColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getDiffuseColor(point, isect);
}


Color CSGUnion::getSpecularColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getSpecularColor(point, isect);
}


CSGIntersection::CSGIntersection(CSGNode *lh, CSGNode *rh)
	: CSGOperation(lh, rh)
{
}

CSGIntersection::~CSGIntersection()
{
}

Intersection CSGIntersection::intersect(const Ray& ray) 
{
	Intersection leftIsect  = left()->intersect(ray);
	Intersection rightIsect = right()->intersect(ray);

	if (!leftIsect.Exists || !rightIsect.Exists)
	{
		return Intersection(false);
	}

	float leftMin  = TOO_FAR_AWAY, leftMax  = -1.f;
	float rightMin = TOO_FAR_AWAY, rightMax = -1.f;

	std::sort(leftIsect.Distances.begin(), leftIsect.Distances.end());
	std::sort(rightIsect.Distances.begin(), rightIsect.Distances.end());

	leftMin  = leftIsect.Distances.front();  leftMax  = leftIsect.Distances.back();
	rightMin = rightIsect.Distances.front(); rightMax = rightIsect.Distances.back();

	Intersection intersect(true);

	if (leftMin < rightMin && leftMax > rightMin)
	{
		intersect.Distance  = rightMin;
		intersect.Object	  = rightIsect.Object;
		intersect.Normal    = rightIsect.Normal;
		intersect.u					= rightIsect.u;
		intersect.v					= rightIsect.v;
		intersect.Distances.push_back(rightMin);
		intersect.Distances.push_back(std::min(leftMax, rightMax));
	}
	else if (rightMin < leftMin && rightMax > leftMin)
	{
		intersect.Distance  = leftMin;
		intersect.Object	  = leftIsect.Object;
		intersect.Normal    = leftIsect.Normal;
		intersect.u					= leftIsect.u;
		intersect.v					= leftIsect.v;
		intersect.Distances.push_back(leftMin);
		intersect.Distances.push_back(std::min(leftMax, rightMax));
	}
	else
	{
		return Intersection(false);
	}

	return intersect;
}

const Material* CSGIntersection::getMaterial() const
{
	return NULL;
}

Vector3D CSGIntersection::getNormal(const Ray& ray, float distance, const Intersection& isect) const
{
	return isect.Normal;
}

void CSGIntersection::setIsLight(bool)
{

}

bool CSGIntersection::isLight() const
{
	return false;
}

Color CSGIntersection::getAmbientColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getAmbientColor(point, isect);
}


Color CSGIntersection::getDiffuseColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getDiffuseColor(point, isect);
}


Color CSGIntersection::getSpecularColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getSpecularColor(point, isect);
}


CSGDifference::CSGDifference(CSGNode *lh, CSGNode *rh)
	: CSGOperation(lh, rh)
{
}

CSGDifference::~CSGDifference()
{
}

Intersection CSGDifference::intersect(const Ray& ray) 
{
	Intersection leftIsect  = left()->intersect(ray);
	Intersection rightIsect = right()->intersect(ray);

	if (!leftIsect.Exists)
	{
		return Intersection(false);
	}
	if (!rightIsect.Exists)
	{
		return leftIsect;
	}

	float leftMin  = TOO_FAR_AWAY, leftMax  = -1.f;
	float rightMin = TOO_FAR_AWAY, rightMax = -1.f;

	std::sort(leftIsect.Distances.begin(), leftIsect.Distances.end());
	std::sort(rightIsect.Distances.begin(), rightIsect.Distances.end());

	leftMin  = leftIsect.Distances.front();  leftMax  = leftIsect.Distances.back();
	rightMin = rightIsect.Distances.front(); rightMax = rightIsect.Distances.back();

	Intersection intersect(true);

	if (rightMax < leftMin)
	{
		return leftIsect;
	}

	if (leftMax < rightMin)
	{
		return leftIsect;
	}

	
	if (leftMin < rightMin)
	{
		intersect.Distance  = leftMin;
		intersect.Object	  = leftIsect.Object;
		intersect.Normal    = leftIsect.Normal; 
		intersect.u					= leftIsect.u;
		intersect.v					= leftIsect.v;

	}
	else if (rightMax < leftMax)
	{
		intersect.Distance  = rightMax;
		intersect.Object	  = rightIsect.Object;
		intersect.Normal    = -rightIsect.Normal;
		intersect.u					= leftIsect.u;
		intersect.v					= leftIsect.v;
	}
	else 
	{
		return Intersection(false);
	}

	intersect.Distances.resize(leftIsect.Distances.size() + rightIsect.Distances.size());
	std::copy(leftIsect.Distances.begin(), leftIsect.Distances.end(), intersect.Distances.begin());
	std::copy(rightIsect.Distances.begin(), rightIsect.Distances.end(), intersect.Distances.begin() + leftIsect.Distances.size());

	return intersect;
}

const Material* CSGDifference::getMaterial() const
{
	return NULL;
}

Vector3D CSGDifference::getNormal(const Ray& ray, float distance, const Intersection& isect) const
{
	return isect.Normal;
}

void CSGDifference::setIsLight(bool)
{
}

bool CSGDifference::isLight() const
{
	return false;
}

Color CSGDifference::getAmbientColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getAmbientColor(point, isect);
}


Color CSGDifference::getDiffuseColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getDiffuseColor(point, isect);
}


Color CSGDifference::getSpecularColor(const Vector3D& point, const Intersection& isect) const
{
	return isect.Object->getSpecularColor(point, isect);
}




