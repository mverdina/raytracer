#ifndef LIGHT_H_
#define LIGHT_H_

#include "types.h"

struct IShape;
class  Ray;
class  Scene;

enum LightSourceType
{
	LIGHTSOURCE_POINT,
	LIGHTSOURCE_DIRECTIONAL,
	LIGHTSOURCE_SPOT
};

struct LightSource
{
	Vector3D Position;
	Vector3D Direction;	
	Color AmbientIntensity;
	Color DiffuseIntensity;
	Color SpecularIntensity;
	float ConstantAttenutaion;
	float LinearAttenutaion;
	float QuadraticAttenutaion;	
	float LightRange; // for directional light
	float PenumbraAngle; // (UmbraAngle, Pi)
	float UmbraAngle; // (0, Pi)
	float SpotlightFalloff;	
	LightSourceType Type; 

	float CosHalfUmbraAngle;
	float CosHalfPenumbraAngle;

	virtual Color computeColor(const Scene& scene, IShape* object, const Ray& viewRay, float distance, const Vector3D& normal) const = 0;
};

struct PointLightSource : LightSource
{
	virtual Color computeColor(const Scene& scene, IShape* object, const Ray& viewRay, float distance, const Vector3D& normal) const;
};

struct DirectionalLightSource : LightSource
{
	virtual Color computeColor(const Scene& scene, IShape* object, const Ray& viewRay, float distance, const Vector3D& normal) const;
};

struct SpotLightSource : LightSource
{
	virtual Color computeColor(const Scene& scene, IShape* object, const Ray& viewRay, float distance, const Vector3D& normal) const;
};

#endif // LIGHT_H_