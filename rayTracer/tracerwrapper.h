#ifndef TRACERWRAPPER_H_
#define TRACERWRAPPER_H_

#include <QImage>
#include <QSharedPointer>
	
class QPainter;
class Scene;

class TracerWrapper
{
public:
	TracerWrapper();
	~TracerWrapper();
	bool loadScene(const QString& fileName);
	void renderScene(int resolutionX, int resolutionY, int width, int height);
	void renderImage(QPainter* painter);
	void saveSceneImage(const QString& fileName);
	void setDepth(int depth);

private:
	QImage mOutput;
	QImage mImage;
	QSharedPointer<Scene> mScene;
	int	mTracerDepth;
};

#endif // TRACERWRAPPER_H_
