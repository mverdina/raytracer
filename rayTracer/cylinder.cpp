#define _USE_MATH_DEFINES
#include <math.h>

#include "material.h"

#include "cylinder.h"

Cylinder::Cylinder(const Vector3D& top, const Vector3D& bottom, float radius, Material* material)
  : mTop(top),
    mBottom(bottom),
    mRadius(radius),
    mMaterial(material),
		mIsLight(false)
{
	mAxis		 = (mTop - mBottom).toUnit();
}

Cylinder::~Cylinder()
{
  delete mMaterial;
}

Intersection Cylinder::intersect(const Ray& ray)
{
	Vector3D ao = ray.getOrigin() - mBottom;
	Vector3D aoab = cross(ao, mAxis);
	Vector3D vab = cross(ray.getDirection(), mAxis);

	float a = dot(vab, vab);
	float rayEnter = -1.f;
	float rayExit = -1.f;
	Intersection res = Intersection(true);
	if (fabs(a) > FLOAT_ZERO)
	{
		float b = 2 * dot(vab, aoab);
		float c = dot(aoab, aoab) - mRadius * mRadius;

		float D = b * b - 4 * a * c;

		if (D < 0.0f) 
		{
			return Intersection(false);
		}

		float t0 = (-b - sqrt(D)) / 2 / a;
		float t1 = (-b + sqrt(D)) / 2 / a;
		if (t0 >= 0.0f)
		{
			Vector3D toBottom = ray.apply(t0) - mBottom;
			Vector3D toTop		= ray.apply(t0) - mTop;
			if (dot(mAxis, toBottom) > 0.0f && dot(mAxis, toTop) < 0.0f)
			{
				res.Distances.push_back(t0);
				rayEnter = t0;
			}
		}
		if (t1 > 0.f)
		{
			Vector3D toBottom = ray.apply(t1) - mBottom;
			Vector3D toTop		= ray.apply(t1) - mTop;
			if (dot(mAxis, toBottom) > 0.0f && dot(mAxis, toTop) < 0.0f)
			{
				res.Distances.push_back(t1);
				if (rayEnter < 0.0f)
				{
					t1 = rayEnter;
				}
				else if (t1 < rayEnter)
				{
					rayExit = rayEnter;
					rayEnter = t1;
				}
				else
				{
					rayExit = t1;
				}
			}
		}
	}
	

	
	if (rayEnter >= 0.f)
	{
		res.Distance = rayEnter;
		res.Object   = this;
		res.Normal	 = getNormal(ray, rayEnter);
		if (rayExit < 0.f) 
		{
			res.InsideIntervals.push_back(Span(0.f, rayEnter));
			res.Distances.insert(res.Distances.begin(), 0.f);
		}
		else
		{
			res.InsideIntervals.push_back(Span(rayEnter, rayExit));
		}
		return res;
	}
  return Intersection(false);
}

Vector3D Cylinder::getNormal(const Ray& ray, float distance, const Intersection& isect) const
{
	const Vector3D atSurface = ray.apply(distance);

	const Vector3D& toBottom = atSurface - mBottom;
	if (fabs(dot(mAxis, toBottom)) < FLOAT_ZERO && dot(toBottom, toBottom) < mRadius * mRadius)
	{
		return -mAxis;
	}
	const Vector3D& toTop = atSurface - mTop;
	if (fabs(dot(mAxis, toTop)) < FLOAT_ZERO && dot(toTop, toTop) < mRadius * mRadius)
	{
		return mAxis;
	}
    
	return (atSurface - mAxis * dot(toBottom, mAxis) - mBottom).toUnit();

}

Color Cylinder::getAmbientColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->AmbientColor;
}

Color Cylinder::getDiffuseColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->DiffuseColor;
}

Color Cylinder::getSpecularColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->SpecularColor;
}
