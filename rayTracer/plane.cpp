#include "material.h"

#define TILE_SIZE 16

#include "plane.h"

Plane::Plane(const Vector3D& normal, float D, Material* material)
	: mNormal(normal),
		mD(D),
		mMaterial(material),
		mIsLight(false)
{
}

Plane::~Plane()
{
	delete mMaterial;
}


Intersection Plane::intersect(const Ray& ray)
{
	float t = -(dot(ray.getOrigin(), mNormal) + mD) / (dot(ray.getDirection(), mNormal));
	if (t > 0.f)
	{
		Intersection res(true, t, this, getNormal(ray, t));
		res.Distances.push_back(t);
		res.InsideIntervals.push_back(Span(t, t));
		return res;
	}
	return Intersection(false);
}

Color Plane::getAmbientColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->AmbientColor;
}

Color Plane::getDiffuseColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->DiffuseColor;
}

Color Plane::getSpecularColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->SpecularColor;
}
