#ifndef RAY_H_
#define RAY_H_

#include "vector3d.h"

class Ray
{
public:
	Ray(const Vector3D& org = Vector3D(), const Vector3D& dir = Vector3D())
		: mOrigin(org), 
			mDirection(dir)
	{
		mDirection.normalize();
	}

	const Vector3D& getOrigin() const
	{
		return mOrigin;
	}

	const Vector3D& getDirection() const
	{
		return mDirection;
	}

	Vector3D apply(float t) const
	{
		return mOrigin + mDirection * t;
	}

private:
	Vector3D mOrigin;
	Vector3D mDirection;
};

#endif // RAY_H_