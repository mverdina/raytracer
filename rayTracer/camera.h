#ifndef CAMERA_H_
#define CAMERA_H_

#include "vector3D.h"
#include "ray.h"

struct CameraProperties
{
	Vector3D Eye;
	Vector3D Up;
	Vector3D At;
	float Fov;
	float NearPlane;
	int	ImagePlaneWidth;
	int	ImagePlaneHeight;
	bool UseExposure;
	bool UseGammaCorrection;
};

class Camera
{
public:

	explicit Camera(const CameraProperties& properties);
	void setImagePlaneRes(int width, int height);
	Ray lookThrough(int x, int y) const;
	bool hasExposure() const
	{
		return mProperties.UseExposure;
	}
	bool hasGammaCorrection() const
	{
		return mProperties.UseGammaCorrection;
	}

private:
	CameraProperties mProperties;
	Vector3D mXAxis;
	Vector3D mYAxis;
	Vector3D mZAxis;
	float mAspectRatio;
	float mFocus;
};

#endif // CAMERA_H_