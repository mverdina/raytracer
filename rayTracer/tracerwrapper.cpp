#include "scene.h"
#include "tracer.h"

#include "sceneserialize.h"
#include "tracerwrapper.h"

TracerWrapper::TracerWrapper()
	: mTracerDepth(0)
{
}

TracerWrapper::~TracerWrapper()
{
}

bool TracerWrapper::loadScene(const QString& fileName)
{
	SceneSerializable reader;
	mScene = reader.readScene(fileName);
	return !!mScene;
}

void TracerWrapper::renderScene(int resolutionX, int resolutionY, int width, int height)
{
	mScene->setImagePlaneRes(resolutionX, resolutionY);
	TracerProperties* tp = mScene->getTracerProperties();
	tp->MaxRayRecursionDepth = mTracerDepth;
	Tracer rayTracer;
	mOutput = QImage(resolutionX, resolutionY, QImage::Format_ARGB32);
	rayTracer.render(*mScene, mOutput.bits());
	mImage = mOutput.scaled(width, height, Qt::KeepAspectRatio);
}

void TracerWrapper::saveSceneImage(const QString& fileName)
{
	mOutput.save(fileName);
}

void TracerWrapper::setDepth(int depth)
{
	mTracerDepth = depth;
}