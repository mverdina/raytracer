#ifndef CONE_H_
#define CONE_H_

#include "shape.h"

#include "ray.h"
#include "vector3d.h"

class Cone : public IShape
{
public:
	Cone(const Vector3D& top, const Vector3D& bottom, float radius, Material* material);
	virtual ~Cone();
	virtual Intersection intersect(const Ray& ray);
	virtual const Material* getMaterial() const
	{
		return mMaterial;
	}

	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

	virtual void setIsLight(bool light)
	{
		mIsLight = light;
	}

	virtual bool isLight() const
	{
		return mIsLight;
	}

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;
private:
	Vector3D	mBottom;
	Vector3D	mTop;
	float     mRadius;
	Vector3D  mAxis;
	float	mRadPerHeight;
	Material* mMaterial;
	bool	mIsLight;
};

#endif // CONE_H_