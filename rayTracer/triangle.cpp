#include "material.h"

#include "triangle.h"
	

Triangle::~Triangle()
{
	delete mMaterial;
}

Intersection Triangle::intersect(const Ray& ray)
{
	const Vector3D& origin	  = ray.getOrigin();
	const Vector3D& direction = ray.getDirection();

	const Vector3D e1 = mV1 - mV0;
	const Vector3D e2 = mV2 - mV0;

	const Vector3D pvec = cross(direction, e2);
	const float		 det  = dot(e1, pvec);

	if (fabs(det) < FLOAT_ZERO)
	{
		return Intersection(false);
	}

	const float invDet = 1.f / det;

	const Vector3D tvec		= origin - mV0;
	float					 lambda = dot(tvec, pvec);

	lambda *= invDet;

	if (lambda < 0.f || lambda > 1.f)
	{
		return Intersection(false);
	}

	const Vector3D qvec = cross(tvec, e1);
	float					 mue	= dot(direction, qvec);

	mue *= invDet;

	if (mue < 0.f || mue + lambda > 1.f)
	{
		return Intersection(false);
	}

	float f = dot(e2, qvec);
	f = f * invDet - FLOAT_ZERO;

	if (f < FLOAT_ZERO)
	{
		return Intersection(false);
	}

	Intersection isect(true, f, this);
	
	isect.Distances.push_back(f);

	isect.u = lambda;
	isect.v = mue;
	isect.Normal    = getNormal(ray, f, isect);

	return isect;
}

Color Triangle::getAmbientColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->AmbientColor;
}

Color Triangle::getDiffuseColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->DiffuseColor;
}

Color Triangle::getSpecularColor(const Vector3D& point, const Intersection& isect) const
{
	return mMaterial->SpecularColor;
}
