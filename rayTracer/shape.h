#ifndef ISHAPE_H_
#define ISHAPE_H_

#include "ray.h"
#include "intersection.h"
#include "types.h"

class Material;


struct IShape
{
	virtual ~IShape() = 0
	{
	}

	virtual Intersection intersect(const Ray& ray) = 0;
	virtual const Material* getMaterial() const = 0;

	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const = 0;
	virtual void setIsLight(bool light) = 0;
	virtual bool isLight() const = 0;

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const = 0;
	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const = 0;
	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const = 0;
};


#endif // ISHAPE_H_