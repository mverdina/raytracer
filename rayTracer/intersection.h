#ifndef INTERSECTION_H_
#define INTERSECTION_H_

#include <vector>
	
#include "vector3d.h"

struct IShape;

struct Span
{	
	explicit Span(float start = -1.f, float end = -1.f)
		: Start(start),
			End(end)
	{

	}

	float Start;
	float End;
};

typedef std::vector<Span> Spans;

struct Intersection
{
	explicit Intersection(bool hit = false, float dist = -1.f, IShape *object = 0x0, const Vector3D& normal = Vector3D())
		: Exists(hit),
			Distance(dist),
			Object(object),
			Normal(normal)
	{
	}
	bool		 Exists;
	float    Distance;
	IShape  *Object;
	Vector3D Normal;

	float		 u;
	float		 v;

	std::vector< float > Distances;
	Spans InsideIntervals;
};


inline bool SpanIsToTheLeft(const Span& lh, const Span& rh)
{
	return lh.End < rh.Start;
}

inline bool SpanIsToTheRight(const Span& lh, const Span& rh)
{
	return lh.Start > rh.End;
}

inline bool operator<(const Span& lh, const Span& rh)
{
	return lh.Start <= rh.Start;
}

inline bool operator>(const Span& lh, const Span& rh)
{
	return lh.Start > rh.Start;
}

inline bool SpanContains(const Span& span, float t)
{
	return span.Start <= t && t <= span.End;
}

Spans SpanUnite(const Span& lh, const Span& rh);

Spans SpanUnite(const Spans& lh, const Spans& rh);

Spans SpanIntersect(const Span& lh, const Span& rh);

Spans SpanIntersect(const Spans& lh, const Spans& rh);

Spans SpanDifference(const Span& lh, const Span& rh);

Spans SpanDifference(const Spans& lh, const Spans& rh);
	

#endif // INTERSECTION_H_