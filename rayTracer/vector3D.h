#ifndef VECTOR3D_H_
#define VECTOR3D_H_

#include <math.h>

#include "precision.h"

class Vector3D
{

	friend Vector3D operator+(const Vector3D& lh, const Vector3D& rh);

	friend Vector3D operator-(const Vector3D& lh, const Vector3D& rh);

	friend Vector3D operator*(const Vector3D& lh, float value);

	friend Vector3D operator*(float value, const Vector3D& rh);

	friend Vector3D operator/(const Vector3D& lh, float value);

	friend Vector3D operator-(const Vector3D& v);

	friend float dot(const Vector3D& lh, const Vector3D& rh);

	friend Vector3D cross(const Vector3D& lh, const Vector3D& rh);

	friend float length(const Vector3D& v);

	friend float length2(const Vector3D& v);
	friend Vector3D scale3D(const Vector3D& lh, const Vector3D& rh);

public:

	Vector3D()
		: mX(0.f),
			mY(0.f),
			mZ(0.f)
	{

	}

	Vector3D(float x, float y, float z)
		: mX(x), mY(y), mZ(z)
	{

	}
	float x() const
	{
		return mX;
	}

	float y() const
	{
		return mY;
	}

	float z() const
	{
		return mZ;
	}

	void setX(float x) 
	{
		mX = x;
	}

	void setY(float y)
	{
		mY = y;
	}

	void setZ(float z)
	{
		mZ = z;
	}

	void setXYZ(float x, float y, float z)
	{
		mX = x; mY = y; mZ = z;
	}

	Vector3D& operator+=(const Vector3D& rh)
	{
		mX += rh.mX;
		mY += rh.mY;
		mZ += rh.mZ;
		return *this;
	}

	Vector3D& operator-=(const Vector3D& rh)
	{
		mX -= rh.mX;
		mY -= rh.mY;
		mZ -= rh.mZ;
		return *this;
	}

	Vector3D& operator*=(float value)
	{
		mX *= value;
		mY *= value;
		mZ *= value;
		return *this;
	}

	Vector3D& operator/=(float value)
	{
		mX /= value;
		mY /= value;
		mZ /= value;
		return *this;
	}

	void normalize()
	{
		float len = length(*this);
		if (len != 0.f)
			*this /= len;
	}

	Vector3D& toUnit()
	{
		normalize();
		return *this;
	}

	Vector3D inverse() const
	{
		return Vector3D(1.f / mX, 1.f / mY, 1.f / mZ);
	}

	Vector3D absolute() const
	{
		return Vector3D(fabs(mX), fabs(mY), fabs(mZ));
	}

private:
	float mX, mY, mZ;
};


inline Vector3D operator+(const Vector3D& lh, const Vector3D& rh)
{
	return Vector3D(lh.mX + rh.mX, lh.mY + rh.mY, lh.mZ + rh.mZ);
}

inline Vector3D operator-(const Vector3D& lh, const Vector3D& rh)
{
	return Vector3D(lh.mX - rh.mX, lh.mY - rh.mY, lh.mZ - rh.mZ);
}

inline Vector3D operator*(const Vector3D& lh, float value)
{
	return Vector3D(lh.mX * value, lh.mY * value, lh.mZ * value);
}

inline Vector3D operator*(float value, const Vector3D& rh)
{
	return Vector3D(rh.mX * value, rh.mY * value, rh.mZ * value);
}

inline Vector3D operator/(const Vector3D& lh, float value)
{
	return Vector3D(lh.mX / value, lh.mY / value, lh.mZ / value);
}

inline Vector3D operator-(const Vector3D& lh)
{
	return Vector3D(-lh.mX, -lh.mY, -lh.mZ);
}

inline float dot(const Vector3D& lh, const Vector3D& rh)
{
	return lh.mX * rh.mX + lh.mY * rh.mY + lh.mZ * rh.mZ;
}

inline Vector3D cross(const Vector3D& lh, const Vector3D& rh)
{
	return Vector3D(lh.mY * rh.mZ - lh.mZ * rh.mY,
					lh.mZ * rh.mX - lh.mX * rh.mZ,
					lh.mX * rh.mY - lh.mY * rh.mX);
}

inline float length(const Vector3D& v)
{
	float len = length2(v);

	if (len < FLOAT_ZERO)
		return 0.f;
	if (len == 1.f)
		return 1.f;

	return sqrtf(dot(v, v));
}

inline float length2(const Vector3D& v)
{
	return dot(v, v);
}

inline Vector3D scale3D(const Vector3D& lh, const Vector3D& rh)
{
	return Vector3D(lh.mX * rh.mX, lh.mY * rh.mY, lh.mZ * rh.mZ);
}

#endif // VECTOR3D_H_