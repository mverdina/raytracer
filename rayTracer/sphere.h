#ifndef SPHERE_H_
#define SPHERE_H_
	
#include "shape.h"

#include "ray.h"
#include "vector3d.h"

class Sphere : public IShape
{
public:

	Sphere(const Vector3D& center, float radius, Material* material);

	virtual ~Sphere();
	virtual Intersection intersect(const Ray& ray);

	virtual const Material* getMaterial() const
	{
		return mMaterial;
	}

	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

	virtual void setIsLight(bool light)
	{
		mIsLight = light;
	}

	virtual bool isLight() const
	{
		return mIsLight;
	}

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

private:
	Vector3D	mCenter;
	float	mRadius;
	Material* mMaterial;
	bool mIsLight;
};

#endif // SPHERE_H_