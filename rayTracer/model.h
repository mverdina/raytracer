#ifndef MODEL_H_
#define MODEL_H_

#include <vector>

#include "shape.h"

#include "bbox.h"
#include "triangle.h"
#include "ray.h"
#include "vector3d.h"

class Triangle;

class Model : public IShape
{
public:
	Model(const std::vector< ModelTriangle* >& triangles, const BBox& bbox, Material* material);

	virtual ~Model();

	virtual Intersection intersect(const Ray& ray);

	virtual const Material* getMaterial() const
	{
		return mMaterial;
	}

	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect = Intersection()) const;

	virtual void setIsLight(bool light)
	{
		mIsLight = light;
	}

	virtual bool isLight() const
	{
		return mIsLight;
	}

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

private:
	std::vector< ModelTriangle* > mTriangles;
	BBox	mBBox;
	Material* mMaterial;
	bool mIsLight;
};

#endif // MODEL_H_