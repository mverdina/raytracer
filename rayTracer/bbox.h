#ifndef BBOX_H_
#define BBOX_H_

#include "vector3D.h"

class Ray;

struct BBox
{
	bool intersect(const Ray& ray) const;
	Vector3D Max;
	Vector3D Min;
};

#endif // BBOX_H_