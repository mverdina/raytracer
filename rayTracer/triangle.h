#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "shape.h"
	
class Triangle : public IShape
{
public:
	explicit Triangle(const Vector3D& v0, const Vector3D& v1, const Vector3D& v2, Material* material)
	: mV0(v0),
		mV1(v1),
		mV2(v2),
		mMaterial(material),
		mIsLight(false)
	{
		Vector3D e1 = v1 - v0;
		Vector3D e2 = v2 - v0;
		mNormal = cross(e1, e2).toUnit();
	}
		

	virtual ~Triangle();
	virtual Intersection intersect(const Ray& ray);

	virtual const Material* getMaterial() const
	{
		return mMaterial;
	}

	virtual Vector3D getNormal(const Ray&, float, const Intersection& isect = Intersection()) const
	{
		return mNormal;
	}

	virtual void setIsLight(bool light)
	{
		mIsLight = light;
	}

	virtual bool isLight() const
	{
		return mIsLight;
	}

	virtual Color getAmbientColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getDiffuseColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

	virtual Color getSpecularColor(const Vector3D& point, const Intersection& isect = Intersection()) const;

private:
	Vector3D mV0;
	Vector3D mV1;
	Vector3D mV2;
	Vector3D mNormal;		
	Material *mMaterial;
	bool mIsLight;
};

class ModelTriangle : public Triangle
{
public:
	explicit ModelTriangle(const Vector3D& v0, const Vector3D& v1, const Vector3D& v2, 
													const Vector3D& n0, const Vector3D& n1, const Vector3D& n2,
													Material* material)
		: Triangle(v0, v1, v2, material),
			mNV0(n0),
			mNV1(n1),
			mNV2(n2)
	{
			
	}

	virtual Vector3D getNormal(const Ray& ray, float distance, const Intersection& isect) const
	{
		Vector3D normal = isect.u * mNV1 + isect.v * mNV2 + (1 - isect.u - isect.v) * mNV0;
		return normal.toUnit();
	}

private:
	Vector3D mNV0;
	Vector3D mNV1;
	Vector3D mNV2;
};

#endif // TRIANGLE_H_